package soft.enager;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author RenRan
 * @createTime 2018-11-15 13:56
 * @description test
 */
public class MapMain {
    public static void main(String[] args) {
        Map map = new HashMap();
        map.put("CAI Huihuang", 5);
        map.put("Zou Encen", 3);
        map.put("Lucy", 5);
        map.put("Lily", 4);
        map.put("Han Meimei", 5);
        map.put("Jim", 3);

        String key = "CAI Huihuang";
        Integer value = null;
        if (map.containsKey(key)) {
            value = (Integer) map.get(key);
            System.out.println("Use key:" + key + " find value:" + value);
        } else {
            System.out.println("Does not contain key:" + key);
        }

        key = "Zou Encen";
        if (map.containsKey(key)) {
            value = (Integer) map.get(key);
            System.out.println("Use key:" + key + " find value:" + value);
        } else {
            System.out.println("Does not contain key:" + key);
        }

        key = "Tom";
        if (map.containsKey(key)) {
            value = (Integer) map.get(key);
            System.out.println("Use key:" + key + " find value:" + value);
        } else {
            System.out.println("Does not contain key:" + key);
        }
        Iterator it = map.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            key = (String) entry.getKey();
            value = (Integer) entry.getValue();
            System.out.println("Use iterator to get the first entry is Key :" + key + "  Value :" + value);
        }

    }
}
