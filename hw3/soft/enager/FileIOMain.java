package soft.enager;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author RenRan
 * @createTime 2018-11-15 13:58
 * @description test
 */
public class FileIOMain {
    private ArrayList outputList = new ArrayList();

    FileIOMain() {
        outputList.add("学号：16200135225");
        outputList.add("姓名：蔡辉煌");
        outputList.add("我爱运动：跑步");
        for (int i = 0; i < 1000; i++) {
            String morning = "Good Morning";
            outputList.add(morning + i);
        }
    }

    public void readFile(String file_path) throws IOException {
        File file;
        FileReader fr;
        BufferedReader br;

        file = new File(file_path);
        if (!file.exists()) {
            System.out.println("\"" + file_path + "\" does not exsit!");
            return;
        }

		/*
		1.FileReader是由java.io.InputStreamReade扩展来的，是针对文件读取的，只能进行底层的字节操作。
		BufferedReader由Reader类扩展而来，提供通用的缓冲方式文本读取，而且提供了很实用的readLine，读取分行文本很适合，BufferedReader是针对Reader的，不直接针对文件，也不是只针对文件读取。
		2.FileReader是用来读文件的类，而BufferReader是将IO流转换为Buffer以提高程序的处理速度。
		*/
        fr = new FileReader(file);
        br = new BufferedReader(fr);

        /*作业：改写以下代码，使用循环读取文件全部内容并打印到屏幕，提示：需要使用BufferedReader.ready()方法，判断文件访问是否到文件末尾*/
		/*String lineText;
		lineText = br.readLine();
		System.out.println(lineText);
		lineText = br.readLine();
		System.out.println(lineText);
		lineText = br.readLine();
		System.out.println(lineText);
		/*作业end*/
        while (br.ready()) {

            System.out.println(br.readLine());
        }

        br.close();
    }

    public void writeFile(String file_path) throws IOException {
        File file;
        FileWriter fw;
        PrintWriter pw;

        file = new File(file_path);
        if (!file.exists()) {
            file.createNewFile();
        }
        fw = new FileWriter(file);
        pw = new PrintWriter(fw);

        /*作业：改写以下代码，使用循环输出FileIOMain.outputList至文件*/
		/*String lineText;
		lineText = "学号：040411214";
		pw.println(lineText);
		lineText = "姓名：邹恩岑";
		pw.println(lineText);
		lineText = "运动：爬山";
		pw.println(lineText);
		/*作业end*/

        Iterator it = outputList.iterator();
        while (it.hasNext()) {
            pw.println(it.next());
        }
        pw.close();
    }

    public static void main(String[] args) throws IOException {
        FileIOMain io = new FileIOMain();
        io.readFile("d:/hw3_input.txt");
        io.writeFile("d:/hw3_output.txt");
    }
}
