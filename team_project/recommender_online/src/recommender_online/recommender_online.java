package recommender_online;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class recommender_online {
	/**存储相似值*/
	HashMap SimMap =new HashMap();
	/**存储影片的详细信息*/
	HashMap movies =new HashMap();
	
	public static void main(String[] args) throws IOException {
		//相似值数据文件地址
		String simPath="simtable.txt";
		//新建类对象
		recommender_online rec = new recommender_online();
		//加载数据
		rec.loadFile(simPath);
		
		System.out.println("Inputed Movie:");
		Scanner sc =new Scanner(System.in);
		String[] movie = sc.nextLine().split("-"); //处理输入的字符串，得到电影ID
		//调用方法得到推荐电影
		rec.selectMovie(movie[0].trim());
	}
	/**
	 * 将数据读入内存
	 * @param filepath
	 * @throws IOException
	 */
	void loadFile(String filepath) throws IOException {
		BufferedReader bur = new BufferedReader(new FileReader(filepath)); //文件读取
		String line;
		//将相似值读入到hashmap
		while((line = bur.readLine()) != null) { //每次读取一行进行处理
			String[] temp = line.split(":");   //字符串分割
			String item = temp[0];  //movieID
			String[] sim = temp[1].split("\t");  //字符串分割
			ArrayList arr = new ArrayList();
			for (String str : sim) {   //遍历得到每一组数据  格式：(movieID,sim)
				 //substring(i,j) 截取从i到j的字符串，去掉两边的括号
				String[] count = str.substring(1 , str.length()-1).split(","); 
				arr.add(count);
			}
			//添加到simmap
			this.SimMap.put(item, arr);
		}
		bur.close(); //关闭流
		
		//将电影信息读入内存
		bur = new BufferedReader(new FileReader("movies.dat"));
		while((line = bur.readLine()) != null) {
			String[] temp = line.split("::");
			movies.put(temp[0], temp);
		}
		bur.close();
	}
	/**
	 * 电影推荐：根据movieID推荐15部电影，按照相似值降序输出详情
	 * @param movieID
	 */
	void selectMovie(String movieID) {
		
		if(SimMap.containsKey(movieID)) {
			//根据movieID获取邻居电影ID和相似值sim
			ArrayList<String[]> item_rait =(ArrayList<String[]>)this.SimMap.get(movieID);
			//排序
			Collections.sort(item_rait,new SortBySim());
			
			//提取前15部电影ID
			ArrayList<String> sim_id = new ArrayList<String>();
			int num=0;
			for (String[] str : item_rait) {
				 sim_id.add(str[0]);
				if(num==14)
					break;
				num++;
			}
			
			//根据电影ID提取详细信息
			ArrayList<String> info = new ArrayList<String>();
			for (String id : sim_id) {
				String[] temp = (String[])movies.get(id);
				info.add(temp[0] + " - " + temp[1] + "::" + temp[2]);
			}

			//输出
			System.out.println("Recommender movies:");
			for (String line : info) {
				System.out.println(line);
			}
		}else {
			System.out.println("此电影不存在，请重新输入。");
		}
		
		

	
		
	}
	
	/**
	 * 重写排序方法
	 * @author 寒涯
	 *
	 */
	class SortBySim implements Comparator {
		@Override
		public int compare(Object o1, Object o2) {
			String[] item1 = (String[])o1;
			String[] item2 = (String[])o2;
	
			if( Float.valueOf(item1[1]) > Float.valueOf(item2[1])) {
				return -1;
			}
			return 1;
		}
	}
	
	
}
