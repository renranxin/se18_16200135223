package recommender_data_process;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.io.BufferedReader;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;
import java.util.Date;

public class DataProcess {
	/**日期格式*/
	SimpleDateFormat dFormat =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**存储user_to_item映射*/
	HashMap UserToItem = new HashMap();
	/**存储item_to_user映射*/
	HashMap ItemToUser = new HashMap();
	/**存储邻居电影*/
	HashMap NeighbourMap = new HashMap();
	/**存储相似值*/
	HashMap simtable = new HashMap();
	/**数据文件路径*/
	String filepath = null;
	/**处理结果保存路径*/
	String savePath = "source\\";
	
	/**
	 * 
	 * @param filepath  测试数据路径
	 * @throws IOException
	 */
	public DataProcess(String filepath) throws IOException {
		this.filepath = filepath;  //获取文件路径
		System.out.println(dFormat.format(new Date(System.currentTimeMillis()))+ "  开始运行，读取文件。。。" );
		LoadFile();
		System.out.println(dFormat.format(new Date(System.currentTimeMillis())) + "  创建邻居映射。。。" );
		setNeighbour();
		System.out.println(dFormat.format(new Date(System.currentTimeMillis())) + "  计算相似值。。。" );
		getSimilarity();
		System.out.println(dFormat.format(new Date(System.currentTimeMillis())) + "  程序运行完毕" );
		
	}

	/**
	 * 加载文件 ；
	 * 生成user_to_item；
	 * 生成item_to_user；
	 */
	void LoadFile() throws IOException {
		//从字符输入流中读取文本
		BufferedReader bufferedReader = new BufferedReader(new FileReader(filepath));
		String line;
		while((line = bufferedReader.readLine()) != null) {  //每次读取一行文本，读到末尾数据为null
			// line = UserID::MovieID::Raiting::Timestamp
			String[] temp = line.split("::");  //temp[0]=userID; temp[1]=movieID; temp[2]=raiting
			
			//填充user_to_item映射
			if(	UserToItem.containsKey(temp[0])) {  //判断UserToItem是否已存在此userID
				ArrayList<String[]> list = (ArrayList<String[]>)UserToItem.get(temp[0]); //提取UserToItem的value
				String[] data = {temp[1] , temp[2]};   // {movieID,Raiting}
				list.add(data);
				UserToItem.put(temp[0] , list);	//在此映射中关联指定值与指定键。如果该映射以前包含了一个该键的映射关系，则旧值被替换。 
			}else { //USerToItem中不存在此UserID,直接增加数据
				ArrayList<String[]> list = new ArrayList<String[]>();
				String[] data = {temp[1] , temp[2]};
				list.add(data);
				UserToItem.put(temp[0] , list);	
			}
			
			//填充item_to_item  原理同上
			if(ItemToUser.containsKey(temp[1])) {
				ArrayList<String[]> list = (ArrayList<String[]>)ItemToUser.get(temp[1]);
				String[] data = {temp[0] , temp[2]};
				list.add(data);
				ItemToUser.put(temp[1] , list);	
			}else {
				ArrayList<String[]> list = new ArrayList<String[]>();
				String[] data = {temp[0] , temp[2]};
				list.add(data);
				ItemToUser.put(temp[1] , list);	
			}
		}
		
		bufferedReader.close(); //关闭数据流
		//保存user_to_item映射
		System.out.println(dFormat.format(new Date(System.currentTimeMillis())) + "  保存user_to_item映射。。。" );
		saveData(savePath+"user_to_item.txt", UserToItem);
		System.out.println(dFormat.format(new Date(System.currentTimeMillis())) + "  user_to_item映射保存成功。。。" );
		//保存item_to_user映射
		System.out.println(dFormat.format(new Date(System.currentTimeMillis())) + "  保存item_to_user映射。。。" );
		saveData(savePath+"item_to_user.txt", ItemToUser);
		System.out.println(dFormat.format(new Date(System.currentTimeMillis())) + "  item_to_user映射保存成功。。。" );
	
	}
	
	/**
	 * 将UserToItem 和 ItemToUser 按照格式保存为txt文件
	 * @param filename 存储路径
	 * @param mapdata 被存储对象
	 * @throws IOException
	 */
	void saveData(String filename,HashMap mapdata) throws IOException {
		
		Iterator iterators = mapdata.entrySet().iterator(); //返回迭代器
		String id;
		ArrayList<String[]> arr;
		//将文本写入字符输出流
		BufferedWriter bw =new BufferedWriter(new FileWriter(filename,false)); //如果为 true，则将字节写入文件末尾处，而不是写入文件开始处
		while(iterators.hasNext()) {
			Map.Entry entry = (Map.Entry) iterators.next(); //返回迭代的下一个元素。 
			id = (String)entry.getKey();  //提取Key
			arr = (ArrayList<String[]>)entry.getValue(); //提取value
			bw.write(id+":");  //先将id写入，然后循环写入value
			int count = arr.size();
			for (String[] str : arr) { //遍历arr中的每一个元素
				String line ="(" + str[0] + "," + str[1] +")" ;
				if(count != 1) {
					line = line + ",";
				}
				bw.write(line);
				count=count-1;
			}
			bw.newLine(); //写入一个行分隔符
		}
		bw.close(); //关闭流
	}
	
	/**
	 * 生成邻居映射
	 */
	void setNeighbour() {
		Iterator item = ItemToUser.entrySet().iterator(); //定义迭代
		String movieID;
		
		while(item.hasNext()) {  //用循环实现迭代
			HashSet<String> tt =new HashSet<String>(); //用hashset存储邻居ID，因为hashset不保存重复值
			Map.Entry entry = (Map.Entry)item.next();  //获取下一个元素
			movieID= (String)entry.getKey();		//获取key
			ArrayList<String[]>  arr = (ArrayList<String[]>)entry.getValue(); //获取相对应的value
			
			for (String[] strings : arr) {  //遍历value中的数组   0:userID 1:Raiting
				//返回此用户观看的所用影片
				ArrayList<String[]> movie_rait_list =(ArrayList<String[]>) UserToItem.get(strings[0]); 
				//将影片ID存入/hashset
				for (String[] strings2 : movie_rait_list) {
					tt.add(strings2[0]);
				}
			}
			//for循环将hashset格式转为arraylist
			tt.remove(movieID);
			ArrayList<String> neighbourlist = new ArrayList<String>();
			for (String string : tt) {
				neighbourlist.add(string);
			}
			//将movieid和邻居列表添加进hashmap
			this.NeighbourMap.put(movieID, neighbourlist);
		}
			
	}
	
	/**
	 * 生成相似值
	 * @throws IOException
	 */
	void getSimilarity() throws IOException {
		Iterator iterators = this.NeighbourMap.entrySet().iterator(); //获取NeighbourMap的迭代

		while(iterators.hasNext()) {
			Map.Entry entry = (Map.Entry)iterators.next(); //获取一组映射
			String movieId = (String)entry.getKey(); //获取movieID
			ArrayList<String> movieList = (ArrayList<String>)entry.getValue(); //获取邻居电影的movieID列表
			ArrayList Vi =new ArrayList();
			ArrayList Vj =new ArrayList();
			ArrayList movie_sim_arr =new ArrayList();
			//从ItemToUser中获取此MovieID的用户和评分数据
			ArrayList<String[]> itemi = (ArrayList<String[]>)ItemToUser.get(movieId); 
			
			for (String idString : movieList) { //遍历每一个邻居电影
				//从ItemToUser中获取此id的用户和评分数据
				ArrayList<String[]> itemj = (ArrayList<String[]>)ItemToUser.get(idString);	
				float sim = Sim(itemi, itemj);	//调用Sim函数计算相似值
				String[] temp= {idString , String.valueOf(sim)};
				movie_sim_arr.add(temp); //将邻居电影ID和相似值sim添加在ArrayList中
			}
			this.simtable.put(movieId, movie_sim_arr);	//将此电影和邻居电影及相似值添加到simtable中
		}
		saveSimilarity(savePath+"simtable.txt" , simtable); //保存simtable
	}
	

	/**
	 * 保存simtable映射
	 * @param filename 保存路径
	 * @param mapdata  被保存数据
	 * @throws IOException
	 */
	void saveSimilarity(String filename,HashMap mapdata) throws IOException {
		
		Iterator iterators = mapdata.entrySet().iterator(); //得到迭代，便于循环
		String id; //movieID
		ArrayList<String[]> arr;  //存储movieid和sim
		//将文本写入字符输出流
		BufferedWriter bw =new BufferedWriter(new FileWriter(filename,false));
		while(iterators.hasNext()) {
			Map.Entry entry = (Map.Entry) iterators.next(); //获取一组映射：key:value
			id = (String)entry.getKey(); //获取Movie ID
			arr = (ArrayList<String[]>)entry.getValue(); //获取ArrayList<String[]>类型数据，
			bw.write(id+":");
			int count = arr.size();
			for (String[] str : arr) { //str={movieID,sim}
				String line ="(" + str[0] + "," + str[1] +")" ;
				if(count != 1) {
					line = line + "\t";
				}
				bw.write(line);
				count=count-1;
			}
			bw.newLine();//写入一个行分隔符
		}
		bw.close(); //关闭流
		
	}
	
	/**
	 * 相似值计算
	 * 先进行维度补零，然后使用余弦相似度计算公式计算两部电影相似度
	 * @param vi ArrayList<String[]>: String[0]为userID,String[1]为raiting
	 * @param vj ArrayList<String[]>: String[0]为userID,String[1]为raiting
	 * @return 返回float类型的相似值
	 */
	float Sim(ArrayList<String[]> vi, ArrayList<String[]> vj)
	{
		ArrayList<Float[]> lis = new ArrayList<Float[]>();//存储两部电影的评分
		//HashSet中不存储重复值，此处保存用户ID
		HashSet users = new HashSet();
		for (String[] str : vi) {
			users.add(str[0]);
		}
		for (String[] str : vj) {
			users.add(str[0]);
		}
		
		/*
		 * 通过循环判断Vi,Vj中是否有此用户的评分
		 * 如果有，将str[1]的分数读取出来
		 * 如果没有，分数为零
		 */
		Iterator iterator = users.iterator();
		float n1 = 0f; //代表0分
		float n2 = 0f; //代表0分
		String id;
		while(iterator.hasNext()) {
			id = (String)iterator.next(); //获取USerID
			n1=0f; //分数归零
			n2=0f; //分数归零
			
			for (String[] str : vi) {
				if(str[0].equals(id)) {
					n1 = Float.valueOf(str[1]); //提取分数
					break;
				}
			}
			
			for (String[] str : vj) {
				if(str[0].equals(id)) { 
					n2 = Float.valueOf(str[1]); //提取分数
					break;
				}
			}
		    Float[] temp = {n1,n2};
		    lis.add(temp); //将一个用户的评分添加进lis中
		}
		
		float simVal = 0;  //相似值
		//处理分子
		float num = 0;
		for (Float[] rait : lis) {
			num += rait[0]*rait[1];
		}
		//处理分母
		float den = 1;
		float a=0;
		float b=0;
		for (Float[] rait : lis) {
			a += rait[0]* rait[0];
			b += rait[1]* rait[1];
		}
		den=(float)Math.sqrt(a*b);
		//得到相似值
		simVal = num / den;
		return simVal;
	}
	
}
