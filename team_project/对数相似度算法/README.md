假设用户1偏好的产品集合(A空间)为k11, k12。 用户1非偏好的产品集合(B空间)为k21, k22.
用户2 在A空间中偏好的产品集合为k11（即与用户1的共同偏好的产品有k11个）,用户2在A空间中非偏好的产品集合即为k12.
用户2在B空间中偏好的产品集合为k21，用户2在B空间中非偏好的集合为k22.
LogLikelihoodSimilarity利用每个空间中的选到偏好的产品概率分布满足二项分布，通过假设A空间与B空间独立不相关H1， 与假设A空间与B空间是相关的H2，计算
log(H) = log( L(H1) / L(H2) )
1. A空间与B空间独立不相关，则用户2的偏好不受用户1影响，用户2 选到喜欢产品的概率为 p = (k11 + k12) / N(按整个空间计算)。
L(H1) = b(k11; k11 + k12; p) * b(k21; k21+k22; p)   //用户2在A空间选到喜欢产品的概率 * 用户2在B空间选到喜欢产品的概率
2. A空间与B空间相关，A与B空间具有不同的偏好概率(即用户1喜欢的，用户2一般也都喜欢，用户1不喜欢的，用户2往往也不会喜欢)。用户2在A空间选到喜欢产品的似然概率为
p1 = k11 / (k11 + k12), 用户2在B空间选到喜欢产品的概率为 p2 = k21 / (k21 + k22)。
L(H2) = b(k11; k11 + k12; p1) * b (k21; k21 + k22, p2)
log(H) = log (L(H1) / L(H2))
Sim = -2 log(H)  // 因为p < p1, p < p2

源代码:
    public static double logLikelihoodRatio(int k11, int k12, int k21, int k22) {
            double rowEntropy = entropy(k11, k12) + entropy(k21, k22);
            double columnEntropy = entropy(k11, k21) + entropy(k12, k22);
            double matrixEntropy = entropy(k11, k12, k21, k22);
            return 2 * (matrixEntropy - rowEntropy - columnEntropy);
        }
        public static double entropy(int... elements) {
            double sum = 0;
            for (int element : elements) {
                sum += element;
            }
            double result = 0.0;
            for (int x : elements) {
                if (x < 0) {
                    throw new IllegalArgumentException(
                        "Should not have negative count for entropy computation: (" + x + ')');
                }
                int zeroFlag = (x == 0 ? 1 : 0);
                result += x * Math.log((x + zeroFlag) / sum);
            }
            return -result;
        }
