#include <iostream>
#include "reverse_string_test.h"
#include "split_string_test.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
using namespace std;

int main(int argc, char** argv) {
	ReverseStringTest::testReverseNormal();
	ReverseStringTest::testReverseBlank();
//	SplitStringTest::testSplitNormal();
//	SplitStringTest::testSplitBlank();
	return 0;
} 
