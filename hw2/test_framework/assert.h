#ifndef ASSERT_H
#define ASSERT_H
#include <iostream>
using std::cout;
using std::endl;

void errorAndExit();

//模板T 可以接收任何类型，如：string, int, float等等 
template <typename T>
void assertEqual(T& left, T& right)
{
	if(left != right)
	{
		errorAndExit();
	}
}

void assert(int res);

#endif

