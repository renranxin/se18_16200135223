package com.cai.test;

class Animal {
	double weight;
	String color;
	Animal(double weight , String color) {
		this.weight = weight;
		this.color = color;
	}
	public String eat() {
		return "Eating...";
	}
	public String sleep() {
		return "Sleep...";
	}
	public String swim() {
		return "Swim...";
	}
	public String info() {
		return ("weight=" + weight + ",color=" + color );
	}
}
class Rabbit extends Animal {
	int age ;
	Rabbit(int age) {
		super(8,"白色 ");
		this.age = age;
	}
	public String eat() {
		return "Eating Radish.. ";
	}
	public String sleep() {
		return " Rabbit Sleep...";
	}
	public String swim() {
		return "Rabbit Swim...";
	}
	public String info() {
		return (super.info()+ ",age=" + age);
	}
}
class Tigger extends Animal {
	String tiggerSex;
	Tigger(String tiggerSex) {
		super(88,"黄色 ");
		this.tiggerSex = tiggerSex;
	}
	public String eat() {
		return "Eating Meat... ";
	}
	public String sleep() {
		return " Tigger Sleep...";
	}
	public String swim() {
		return "Tigger Swim...";
	}
	public String info() {
		return (super.info() + ",tiggerSex=" + tiggerSex );
	}
}
public class TestAnimal {
	public static void main(String [] args) {
		Rabbit rabbit = new Rabbit(9);
		Tigger tigger = new Tigger(" 男");
		/*
		rabbit.weight = 8;
		rabbit.color = " 白色 ";
		tigger.weight = 88;
		tigger.color = " 黄色 ";
		*/
		System.out.println(rabbit.info());
		System.out.println("rabbit.eat=" + rabbit.eat());
		System.out.println("rabbit.sleep=" + rabbit.sleep());
		System.out.println("rabbit.swim=" + rabbit.swim());
		System.out.println("==========================");
		System.out.println(tigger.info());
		System.out.println("tigger.eat=" + tigger.eat());
		System.out.println("tigger.sleep=" + tigger.sleep());
		System.out.println("tigger.swim=" + tigger.swim());
	}
}