package com.cai.test;

public class Sushu {
	   public int getSum(int n)
	   {
	      int sum=0,i;
	      for(i=1;i<=n;i+=2)
	      {
	        sum+=i;
	      }
	      return sum;
	   } 
	   public void displayPrime(int n)
	   {
	      int i,j,flag=0;
	      for(i=2;i<=n;i++)
	      {
	         flag=0;
	         for(j=2;j<=i/2;j++)
	           if(i%j==0)
	           {
	              flag=1;
	              break;
	           }
	         if(flag==0)
	         {
	            System.out.printf("%d ",i); 
	         } 
	      }
	   }
	   public static void main(String[] args)
	   {
		   Sushu t=new Sushu();
	       System.out.println("50以内的奇数和为:"+t.getSum(50));
	       System.out.println("50以内的素数:");
	       t.displayPrime(50);
	   }
}
