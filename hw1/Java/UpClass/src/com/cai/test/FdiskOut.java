package com.cai.test;

import java.util.Scanner;

public class FdiskOut {
	public static void main(String[] args) {
		
		//创建一个jdk中用于获取终端输入信息的工具，并且定义了一个变量sc来代表这个创建出来的工具
		Scanner sc = new Scanner(System.in);
		
		//用工具从终端上获取用户输入的一行文字
		String name = sc.nextLine();
		
		//拼接新的字符串
		String res = name +"aaaa,"+name+"bbb,"+name+"ccc.";
		
		//将拼接的结果打印到终端 
		System.out.println(res);
	}
	
	
}
